//
//  ModelParseTest.swift
//  WeatherForecastDemoTests
//
//  Created by Fong Bao on 13/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import XCTest
@testable import WeatherForecastDemo

class ModelParseTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testFromJson() {
        
        let filePath = Bundle(for: ModelParseTest.self).url(forResource: "WeatherDataResponse", withExtension: "json")
        
        let loginData = retrieveDataFromBundle(fileUrl: Bundle(for: ModelParseTest.self).url(forResource: "WeatherDataResponse2", withExtension: "json"))
        XCTAssertNotNil(loginData, "LoginData should not be nil")
        
        
    }
    
    func testAPIResponse() {
        
        let expectedRespnseCode = "200"
        let expectedCity = "London"
        let expectedWeatherMain = "Clouds"
        let expectedWeatherDescription = "scattered clouds"
        
        let loginData = retrieveDataFromBundle(filePath: Bundle.main.path(forResource: "WeatherDataResponse", ofType:"json"))
        XCTAssertNotNil(loginData, "LoginData should not be nil")
        
        guard let data = loginData else {
            XCTFail("data is nil")
            return
        }
        
        guard let apiResponse = try? JSONDecoder().decode(APIResults.self, from: data) else {
            XCTFail("weather decoder error")
            return
        }
        
        guard apiResponse.code == expectedRespnseCode else {
            XCTFail("weather decoder error")
            return
        }
        
        guard apiResponse.city.name == expectedCity else {
            XCTFail("weather decoder error")
            return
        }

        guard apiResponse.list?.first?.weather.first?.main == expectedWeatherMain else {
            XCTFail("weather decoder error")
            return
        }

        guard apiResponse.list?.first?.weather.first?.description == expectedWeatherDescription else {
            XCTFail("weather decoder error")
            return
        }
        
        
    }

    
    func retrieveDataFromBundle(fileUrl: URL?) -> Data? {
        guard let path = fileUrl else {
            print("Invalid filename/path.")
            return nil
        }
        guard let data = try? Data(contentsOf: path, options: Data.ReadingOptions.uncached) else {
            print("Failed to convert into Data")
            return nil
        }
        return data
    }
    
    func retrieveDataFromBundle(filePath: String?) -> Data? {
        guard let path = filePath else {
            print("Invalid filename/path.")
            return nil
        }
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: Data.ReadingOptions.uncached) else {
            print("Failed to convert into Data")
            return nil
        }
        return data
    }
    

}
