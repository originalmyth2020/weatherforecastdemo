Was built with Xcode 10.0 
Using Swift 4.2

Libraries included RxSwift, Realm, SDWebImage (Only used SDWebImage at this point)

Requires Cocoapods 1.0.0 + Need to run pod install on project

Improvements
Metric option is hard coded. Could set options to change in settings
Activity Indicator not done 

Display current weather view in large uiview
Only fiveDay weather call is done. This leaves daily and sixteen day to do.
Improved error handling for network calls
Placeholder Image

Could show saved searches when typing in search string
Could implement Predictive Search

Note, Fong Bao is the name of my computer I did the work on.