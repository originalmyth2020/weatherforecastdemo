//
//  WeatherData.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 14/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//
import Foundation
import UIKit
import SDWebImage

class WeatherData {
    
    let weatherDescription: APIWeatherDescription
    let timeString: String
    let temp: String
    private var icon: UIImage?
    
    init(weatherDescription: APIWeatherDescription, timeString: String, temp: String) {
        self.weatherDescription = weatherDescription
        self.timeString = timeString
        self.temp = temp
    }
    
    func iconImage(completion: @escaping (UIImage) -> Void) {
        if let iconImage = icon {
            completion(iconImage)
        } else {
            guard let urlString = (APIConstants.BASE_PNG_STRING + weatherDescription.icon + APIConstants.END_PNG_STRING).urlEncode() else {
                //TODO: add placeholder Image
                completion(UIImage(named: "") ?? UIImage())
                return
            }
            guard let url = URL(string: urlString) else {
                //TODO: add placeholder Image
                completion(UIImage(named: "") ?? UIImage())
                return
            }
            SDWebImageManager.shared().loadImage(with: url, options: [], progress: nil, completed: { [weak self] image, _, error, _, isDownloaded, url in
                if let iconImage = image {
                    self?.icon = iconImage
                    completion(iconImage)
                } else {
                    //TODO: add placeholder Image
                    completion(UIImage(named: "") ?? UIImage())
                }
            })
            
        }
    }
    
}
