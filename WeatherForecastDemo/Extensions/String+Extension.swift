//
//  String+Extension.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 14/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation

extension String {
    
    func urlEncode() -> String? {
        return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    }
    
    //DateFormatter
    func dateFromString(dateFormatter: DateFormatter) -> Date {
        guard let date = dateFormatter.date(from: self) else {
            return Date()
        }
        return date
    }
    
}
