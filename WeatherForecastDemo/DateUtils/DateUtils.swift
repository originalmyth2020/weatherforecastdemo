//
//  DateUtils.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 14/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation

enum WeekDay: Int {
    
    case Sunday = 1
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
    
    var description : String {
        switch self {
        case .Sunday:
            return "Sunday"
        case .Monday:
            return "Monday"
        case .Tuesday:
            return "Tuesday"
        case .Wednesday:
            return "Wednesday"
        case .Thursday:
            return "Thursday"
        case .Friday:
            return "Friday"
        case .Saturday:
            return "Saturday"
        }
    }
    
}

class DateUtils {
    
    static let dateFormatter = DateFormatter()
    
    private init() {
        //convinence class
    }
    
    static func weekdayFrom(dateString: String) -> WeekDay? {
        let dateFormat = "dd-MM-yyyy"
        dateFormatter.dateFormat = dateFormat
        return WeekDay.init(rawValue: Calendar.current.component(.weekday, from: dateString.dateFromString(dateFormatter: dateFormatter)))
    }
    
}
