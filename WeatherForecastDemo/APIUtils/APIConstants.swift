//
//  APIConstants.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 13/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation

//Weather call types
enum WeatherFunction {
    case daily
    case fiveDay
    case sixteenDay
}

struct APIConstants {
    
    static let BASE_STRING = "https://api.openweathermap.org"
    static let PNG_STRING = "/img/w/"
    static let FIVE_DAY_FORSECAST = "/data/2.5/forecast?"
    static let COUNTRY_CODE = ",uk"
    static let METRIC_UNITS = "&units=metric"
    static let APP_KEY = "&appid="
    static let KEY_ACTUAL = "ac328bb561a9a0600fbecb3e70ca96dc"
    static let BY_CITY_NAME = "q="
    static let  BASE_PNG_STRING = "https://openweathermap.org/img/w/"
    static let  END_PNG_STRING = ".png"
    
}

class APIEndpoints {
    
    static func fetchWeatherForecastUrl<T>(location: T, weatherFunction: WeatherFunction) -> URL? {
        
        var urlString = APIConstants.BASE_STRING + APIConstants.FIVE_DAY_FORSECAST
        
        switch location {
        case is String:
            urlString.append(APIConstants.BY_CITY_NAME + (location as? String ?? "") + APIConstants.COUNTRY_CODE)
        case is APICoordinate:
            if let location = location as? APICoordinate {
                urlString.append("lat=\(location.lat)&lon=\(location.lon)")
            }
        default:
            return nil
        }
        
        switch weatherFunction {
        case .daily:
            //TODO: Not implemented yet
            return nil
        case .fiveDay:
            urlString.append(APIConstants.METRIC_UNITS + APIConstants.APP_KEY + APIConstants.KEY_ACTUAL)
        case .sixteenDay:
            //TODO: Not implemented yet
            return nil
        }
        
        guard let fullUrlString = urlString.urlEncode() else {
            return nil
        }
        
        return URL(string: fullUrlString)
    }
    
}

