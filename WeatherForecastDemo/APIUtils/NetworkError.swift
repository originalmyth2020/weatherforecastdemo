//
//  NetworkError.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 14/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation

/*
 To expand
 */
enum NetworkError : Error {
    case networkError
    case conversionError
}
