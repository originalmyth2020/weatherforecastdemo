//
//  MainViewController.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 13/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import UIKit
import CoreLocation

class HoursWeatherViewCell: UICollectionViewCell {
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
}


class MainViewController: UIViewController {
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var searchTableView: UITableView!
    
    let viewModelLayer: ViewModelLayer = ViewModelLayer(serviceManager: ServiceManager())
    var locationManager: CLLocationManager?
    var weatherDict: [String: [WeatherData]]?
    var weatherDaysArray = [String]()
    var searchText: String?
    private let reuseIdentifier = "weatherViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let width = view.frame.width
        flowLayout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 5, right: 5)
        flowLayout.itemSize = CGSize(width: width, height: width/3)
        mainCollectionView.setCollectionViewLayout(flowLayout, animated: true)
        searchTableView.isHidden = true
        searchBar.delegate = self
        locationManager = CLLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager?.delegate = self
        locationManager?.allowsBackgroundLocationUpdates = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        locationManager?.requestAlwaysAuthorization()
    }

    // MARK: ViewModelLayer Methods
    private func getWeatherByLocation<T>(location: T) {
        searchTableView.isHidden = true
        searchBar.resignFirstResponder()
         self.locationManager?.stopUpdatingLocation()
        viewModelLayer.fetchThreeHourForecastFor(location: location, completion: { [weak self] error, weatherDict, weatherArray, city in
        
            guard error == nil, let weatherDict = weatherDict, let weatherArray = weatherArray else {
                return
            }
            self?.weatherDict = weatherDict
            self?.weatherDaysArray = weatherArray
            DispatchQueue.main.async { [weak self] in
                if let searchLocation = city?.name {
                    self?.locationLabel.text = searchLocation
                }
                self?.mainCollectionView.reloadData()
                if self?.weatherDaysArray.count ?? 0 <= 0  {
                    self?.showAlert(message: "No results found for location!")
                }
            }
        })
    }
    
    func showAlert(message: String) {
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        let sendButton = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
        })
        alertController.addAction(sendButton)
        self.present(alertController, animated: true, completion: nil)
    }
}

extension MainViewController: UICollectionViewDataSource {
    // MARK: - CollectionView
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerView", for: indexPath) as? WeatherHeaderView else {
                return UICollectionReusableView()
            }
            let date = weatherDaysArray[indexPath.section]
            var day = ""
            if let forcastDay = DateUtils.weekdayFrom(dateString: date)?.description {
                day = "\(forcastDay) "
            }
            headerView.headerLabel.text = "\(day)\(date)"
            return headerView
        default:
            assert(false, "Unexpected element kind")
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return weatherDaysArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? WeatherViewCell else {
            return UICollectionViewCell()
        }
        let sectionTitle = weatherDaysArray[indexPath.section]
        if let mWeatherDict = weatherDict {
            cell.setUp(data: mWeatherDict[sectionTitle] ?? [])
        }
        return cell
    }
}

extension MainViewController: UICollectionViewDelegate {
    
}

extension MainViewController: UISearchBarDelegate {
    // MARK: UISearchBarDelegate methods
    //TODO: Saved Searched, Predictive Search
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchTableView.isHidden = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchText = ""
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let search = searchText, search.count > 0 else {
            showAlert(message: "Search text is empty!")
            return
        }
        getWeatherByLocation(location: search)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchText = searchText
    }
    
}

extension MainViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let defaultLocation = CLLocation(latitude: 51.509865, longitude: -0.118092)
        let latestLocation : CLLocation
        if let retrivedLocation = locations.last {
            let maxAge:TimeInterval = 60
            let requiredAccuracy: CLLocationAccuracy = 100
            let locationIsValid: Bool = Date().timeIntervalSince(retrivedLocation.timestamp) < maxAge && retrivedLocation.horizontalAccuracy <= requiredAccuracy
            guard locationIsValid else {
                return
            }
            print("latest location is...", retrivedLocation)
            latestLocation = retrivedLocation
        } else {
            latestLocation = defaultLocation
        }
        getWeatherByLocation(location: APICoordinate(lat: latestLocation.coordinate.latitude, lon: latestLocation.coordinate.longitude))
        
    }

    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            //TODO if auth fails add Alert
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            locationManager?.startUpdatingLocation()
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager?.stopUpdatingLocation()
        print("Location Error: \(error)")
    }
}
