//
//  HoursWeatherView.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 13/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation
import UIKit


class WeatherHeaderView: UICollectionReusableView {
    @IBOutlet weak var headerLabel: UILabel!
}

class WeatherViewCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate  {
    
    private let reuseHoursIdentifier = "hoursCell"
    var hoursArray: [WeatherData] = [WeatherData]()
    
    @IBOutlet weak var hoursLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var hoursCollectionView: UICollectionView!
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hoursArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseHoursIdentifier, for: indexPath) as? HoursWeatherViewCell else {
            return UICollectionViewCell()
        }
        let weatherData = hoursArray[indexPath.row]
        cell.hoursLabel.text = weatherData.timeString
        cell.tempLabel.text = weatherData.temp
        cell.iconImage.image = nil
        weatherData.iconImage(completion: { image in
            DispatchQueue.main.async {
                cell.iconImage.image = image
            }
        })
        cell.backgroundColor = UIColor.cyan
        
        return cell
    }
    
    func setUp(data: [WeatherData]) {
        hoursCollectionView.delegate = self
        hoursCollectionView.dataSource = self
        hoursArray = data
        hoursCollectionView.reloadData()
    }
    
}

