//
//  ViewModelLayer.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 14/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation


class ViewModelLayer: ViewModelProtocol {
    
    var serviceManager: ServiceManagerProtocol?
    
    init(serviceManager: ServiceManagerProtocol) {
        self.serviceManager = serviceManager
    }

    func fetchThreeHourForecastFor<T>(location: T, completion: @escaping (NetworkError?, [String: [WeatherData]]?, [String]?, APICity?) -> Void) {
        serviceManager?.fetchForecast(location: location, completion: { [weak self] response, result in
            guard response == nil else {
                completion(nil, nil, nil, nil)
                return
            }
            guard let weatherSegment = result?.list else {
                completion(nil, nil, nil, nil)
                return
            }
            self?.convertResponse(weatherSegment: weatherSegment, completion: { segmentsDict, daysArray in
                completion(nil, segmentsDict, daysArray, result?.city)
            })
        })
    }

    //NetworkError?,
    private func convertResponse(weatherSegment: [APIWeatherSegment], completion: @escaping ([String: [WeatherData]], [String]) -> Void) {
        
        var segmentsDict = [String: [WeatherData]]()
        var daysArray = [String]()
        let separatorSet = CharacterSet(charactersIn: " -")
        
        for item in weatherSegment {
            let comps = item.dateString.components(separatedBy: separatorSet)
            let date = "\(comps[2])-\(comps[1])-\(comps[0])"
            let temp = "Temp: \(item.main.temp) °C"
            let timeComps = comps[3].components(separatedBy: ":")
            let time = "\(timeComps[0]):\(timeComps[1])"
            
            guard let weatherItem = item.weather.first else {
                continue
            }
            let weatherData = WeatherData(weatherDescription: weatherItem, timeString: time, temp: temp)
            
            if var dayDictEntry = segmentsDict[date] {
                dayDictEntry.append(weatherData)
                segmentsDict[date] = dayDictEntry
            } else {
                daysArray.append(date)
                segmentsDict[date] = [weatherData]
            }
        }
        completion(segmentsDict, daysArray)
    }

    
}
