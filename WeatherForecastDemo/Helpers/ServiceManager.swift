//
//  ServiceManager.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 13/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation
import UIKit


enum HttpMethodType: String {
    case GET
    case POST
    case DELETE
    case PUT
}


class ServiceManager: ServiceManagerProtocol {
    
    typealias DataRaw = (Data?, URLResponse?, Error?) -> Void

    func fetchForecast<T>(location: T, completion: @escaping (NetworkError?, APIResults?) -> Void) {
        
        guard let url = APIEndpoints.fetchWeatherForecastUrl(location: location, weatherFunction: .fiveDay) else {
            print("Failed to create URL")
            completion(NetworkError.networkError, nil)
            return
        }
        request(url: url, completion: { data, response, error in
            guard error == nil, let data = data else {
                print("fetchForecast error")
                completion(NetworkError.networkError, nil)
                return
            }
            guard let result = try? JSONDecoder().decode(APIResults.self, from: data) else {
                print("fetchForecast decoder error")
                completion(NetworkError.networkError, nil)
                return
            }
            completion(nil, result)
        })
    }
    
    private func request(url: URL, httpType: HttpMethodType = .GET, headers: [String : String]? = nil, parameters: Data? = nil, completion: @escaping DataRaw) {
        let sessionConfiguration = URLSessionConfiguration.default
        var request = URLRequest(url: url)
        request.httpMethod = httpType.rawValue
        request.httpBody = parameters
        request.allHTTPHeaderFields = headers
        let urlSession = URLSession(configuration:sessionConfiguration, delegate: nil, delegateQueue: nil)
        let sessionTask = urlSession.dataTask(with: request) { (data, response, error) in
            completion(data, response, error)
        }
        sessionTask.resume()
    }
    
}
