//
//  ViewModelProtocol.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 14/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation

protocol ViewModelProtocol {
    
    func fetchThreeHourForecastFor<T>(location: T, completion: @escaping (NetworkError?, [String: [WeatherData]]?, [String]?, APICity?) -> Void)
    
}
