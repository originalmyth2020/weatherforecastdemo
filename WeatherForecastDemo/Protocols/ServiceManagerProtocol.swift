//
//  ServiceManagerProtocol.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 13/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation

protocol ServiceManagerProtocol {
    
    func fetchForecast<T>(location: T, completion: @escaping (NetworkError?, APIResults?) -> Void)
    
}
