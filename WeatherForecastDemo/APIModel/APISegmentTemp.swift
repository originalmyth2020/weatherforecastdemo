//
//  APISegmentTemp.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 13/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation

struct APISegmentTemp: Codable {
    
    let temp: Double
    let tempMin: Double
    let tempMax: Double
    let humidity: Int
    
    enum CodingKeys: String, CodingKey {
        case temp
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case humidity
    }
    
}
