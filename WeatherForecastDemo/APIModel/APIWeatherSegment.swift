//
//  APIWeatherSegment.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 13/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation

struct APIWeatherSegment: Codable {
    
    let dateString: String
    let main: APISegmentTemp
    let weather: [APIWeatherDescription]
    
    enum CodingKeys: String, CodingKey {
        case dateString = "dt_txt"
        case main
        case weather
    }
}
