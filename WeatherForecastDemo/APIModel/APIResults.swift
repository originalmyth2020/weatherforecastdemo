//
//  APIResults.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 13/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation

struct APIResults: Codable {
    
    var list: [APIWeatherSegment]?
    let code: String
    let city: APICity
    
    enum CodingKeys: String, CodingKey {
        case code = "cod"
        case list
        case city
    }
    
}
