//
//  APICity.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 13/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation


struct APICity: Codable {
    let id: Int
    let name: String
    let coord: APICoordinate
}
