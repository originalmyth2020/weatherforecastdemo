//
//  APICoordinate.swift
//  WeatherForecastDemo
//
//  Created by Fong Bao on 13/10/2018.
//  Copyright © 2018 Duncan. All rights reserved.
//

import Foundation

struct APICoordinate: Codable {
    let lat: Double
    let lon: Double
}
